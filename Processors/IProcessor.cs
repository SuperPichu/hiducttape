namespace HIDuctTape.Processors
{
	public class IProcessor<T>
	{
		public virtual T Execute(T input)
		{
			return input;
		}
	}
}
