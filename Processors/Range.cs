using HIDuctTape.Controllers.Handlers;

namespace HIDuctTape.Processors
{
	public class Range : IProcessor<InputEvent>
	{
		private short Max;

		private short Min;

		public Range(short max = short.MaxValue, short min = short.MinValue)
		{
			Max = max;
			Min = min;
		}

		public override InputEvent Execute(InputEvent input)
		{
			input.Value = ConvertRange(input.Value, Min, Max, short.MinValue, short.MaxValue);
			return input;
		}

		public static short ConvertRange(short value, short originalStart, short originalEnd, short newStart, short newEnd)
		{
			double num = (double)(newEnd - newStart) / (double)(originalEnd - originalStart);
			return (short)((double)newStart + (double)(value - originalStart) * num);
		}
	}
}
