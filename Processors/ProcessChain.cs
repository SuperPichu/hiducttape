using System.Collections.Generic;

namespace HIDuctTape.Processors
{
	public class ProcessChain<T>
	{
		private Queue<IProcessor<T>> Processors = new Queue<IProcessor<T>>();

		public ProcessChain()
		{
			AddProcessor(new IProcessor<T>());
		}

		public T Execute(T input)
		{
			foreach (IProcessor<T> processor in Processors)
			{
				input = processor.Execute(input);
			}
			return input;
		}

		public void AddProcessor(IProcessor<T> processor)
		{
			Processors.Enqueue(processor);
		}
	}
}
