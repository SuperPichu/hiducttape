using HIDuctTape.Controllers.Handlers;

namespace HIDuctTape.Processors
{
	public class ButtonsToAxis : IProcessor<InputEvent>
	{
		private short Off;

		private short On;

		private bool Multi = false;

		public ButtonsToAxis(short off = 0, short on = short.MaxValue, bool multi = false)
		{
			Off = off;
			On = on;
			Multi = multi;
		}

		public override InputEvent Execute(InputEvent input)
		{
			if (Multi)
			{
				if (input.Index == Off)
				{
					if (input.Value > 0)
					{
						input.Value = short.MinValue;
					}
					else
					{
						input.Value = 0;
					}
				}
				else if (input.Index == On)
				{
					if (input.Value > 0)
					{
						input.Value = short.MaxValue;
					}
					else
					{
						input.Value = 0;
					}
				}
				return input;
			}
			bool flag = input.Value > 0;
			input.Value = Off;
			if (flag)
			{
				input.Value = On;
			}
			return input;
		}
	}
}
