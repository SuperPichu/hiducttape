using HIDuctTape.Controllers.Handlers;
using System;

namespace HIDuctTape.Processors
{
	public class Invert : IProcessor<InputEvent>
	{
		public override InputEvent Execute(InputEvent input)
		{
			if (input.OutProp.FieldType == typeof(short))
			{
				input.Value = (short)(-1 * input.Value);
			}
			else if (input.OutProp.FieldType == typeof(bool))
			{
				bool flag = Convert.ToBoolean(input.Value);
				input.Value = Convert.ToInt16(!Convert.ToBoolean(input.Value));
			}
			return input;
		}
	}
}
