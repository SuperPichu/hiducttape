using HIDuctTape.Controllers.Handlers;

namespace HIDuctTape.Processors
{
	public class AxisToButton : IProcessor<InputEvent>
	{
		private short Max;

		private short Min;

		public AxisToButton(short max = short.MaxValue, short min = short.MinValue)
		{
			Max = max;
			Min = min;
		}

		public override InputEvent Execute(InputEvent input)
		{
			if (input.Value <= Max && input.Value >= Min)
			{
				input.Value = 1;
			}
			else
			{
				input.Value = 0;
			}
			return input;
		}
	}
}
