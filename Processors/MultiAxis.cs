using HIDuctTape.Controllers.Handlers;

namespace HIDuctTape.Processors
{
	public class MultiAxis : IProcessor<InputEvent>
	{
		private int Axis1;

		private string Type1;

		private int Axis2;

		private string Type2;

		public MultiAxis(int axis1, string type1, int axis2, string type2)
		{
			Axis1 = axis1;
			Axis2 = axis2;
			Type1 = type1;
			Type2 = type2;
		}

		public override InputEvent Execute(InputEvent input)
		{
			if (input.Index == Axis1 && Type1 == "max")
			{
				input.Value = ConvertRange(input.Value, short.MaxValue, short.MinValue, 0, short.MaxValue);
			}
			if (input.Index == Axis2 && Type2 == "max")
			{
				input.Value = ConvertRange(input.Value, short.MaxValue, short.MinValue, 0, short.MinValue);
			}
			return input;
		}

		public static short ConvertRange(short value, short originalStart, short originalEnd, short newStart, short newEnd)
		{
			double num = (double)(newEnd - newStart) / (double)(originalEnd - originalStart);
			return (short)((double)newStart + (double)(value - originalStart) * num);
		}
	}
}
