﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
#if WINDOWS
using Nefarius.ViGEm.Client;
using Nefarius.ViGEm.Client.Targets;
#endif
using HIDuctTape.Controllers;
using HIDuctTape.Gamepad;
using HIDuctTape.Controllers.Handlers;
using Newtonsoft.Json;
using Serilog;
using HIDuctTape.Controllers.Mapping;

namespace HIDuctTape
{
    class Program
    {
        static List<TrashPad> TrashPads = new List<TrashPad>();

#if WINDOWS
        static ViGEmClient client;
#endif

        static bool Connected = false;

        static void Main2(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
            .WriteTo.Console().WriteTo.File("log.txt")
            .CreateLogger();
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
            .WriteTo.Console().WriteTo.File("log.txt")
            .CreateLogger();
#if WINDOWS
            client = new ViGEmClient();
            directInput = new DirectInput();
#endif
            GlobalConfig.Current = JsonConvert.DeserializeObject<GlobalConfig>(File.ReadAllText("configs/global.json"));
            List<string> configs = Directory.GetFiles("configs", "*.json").ToList();
            configs.Remove("configs/global.json");
            for (int i = 1; i <= GlobalConfig.Current.Players; i++)
            {
                foreach (string file in configs)
                {
                    Console.WriteLine($"{configs.IndexOf(file)}:{file}");
                }
                Console.Write($"Choose config for Player {i}: ");
                int choice = Int32.Parse(Console.ReadLine());
                TrashPad trashPad = new TrashPad(configs[choice], i);
                foreach (ControllerDef c in trashPad.Mapping.Controllers)
                {
                    switch (c.Type.ToLower())
                    {
                        case "gamepad":
                            string device = ChooseGamepad(c.Name);
                            GamepadController controller = new GamepadController(device,c);
                            GamepadHandler gamePadHandler = new GamepadHandler(trashPad, controller);
                            Thread newThread = new Thread(trashPad.ProcessQueue);
                            newThread.Start();
                            break;

                    }
                }
                TrashPads.Add(trashPad);
            }
            while (true)
            {
                Console.WriteLine("Type exit to exit");
                string i = Console.ReadLine().ToLower();
                if (i.Equals("exit"))
                {
                    foreach (TrashPad trashPad in TrashPads){
                        trashPad.Disconnect();
                    }
                    break;
                }
            }
        }
        static string ChooseGamepad(string name)
        {
            Console.WriteLine("Gamepads:");
            // for(int i = 0;i < 4;i++){
            //     if(GamePad.GetState(i).IsConnected) Console.WriteLine($"\t{i}:{GamePad.GetCapabilities(i).DisplayName}");
            // }
            string[] options = Directory.GetFiles("/dev/input/", "js?");
            for (int i = 0; i < options.Length; i++)
            {
                string c = options[i];
                //string n = GamepadController.GetName(c);
                Console.WriteLine($"\t{i}:- {c}");
            }

            Console.Write($"Choose device for controller \"{name}\": ");
            int choice = Int32.Parse(Console.ReadLine());
            return options[choice];
        }
        // static int ChooseJoystick(TrashPad trashPad)
        // {
        //     Console.WriteLine("Joysticks:");
        //     for(int i = 0;i < 4;i++){
        //         if(Joystick.GetState(i).IsConnected) Console.WriteLine($"\t{i}:{Joystick.GetCapabilities(i).Identifier}");
        //     }
        //     Console.Write("Choose: ");
        //     int choice = Int32.Parse(Console.ReadLine());
        //     return choice;
        // }
    }
}
