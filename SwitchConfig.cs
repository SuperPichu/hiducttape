namespace HIDuctTape
{
	public class SwitchConfig
	{
		public string Ip
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public int Player
		{
			get;
			set;
		}
	}
}
