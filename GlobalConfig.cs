using Newtonsoft.Json;
using System.Collections.Generic;

namespace HIDuctTape
{
	public class GlobalConfig
	{
		public bool UseSwitch = false;
		
		public int Players = 1;

		public static GlobalConfig Current
		{
			get;
			set;
		}

		[JsonProperty("switchs")]
		public List<SwitchConfig> SwitchConfigs
		{
			get;
			set;
		}
	}
}
