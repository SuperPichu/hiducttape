namespace HIDuctTape.Controllers
{
	public class ControllerState
	{
		public short LX = 0;

		public short LY = 0;

		public short RX = 0;

		public short RY = 0;

		public bool A = false;

		public bool B = false;

		public bool X = false;

		public bool Y = false;

		public bool LeftShoulder = false;

		public bool RightShoulder = false;

		public bool RightStick = false;

		public bool LeftStick = false;

		public short LT = 0;

		public bool ZL = false;

		public short RT = 0;

		public bool ZR = false;

		public bool Start = false;

		public bool Back = false;

		public bool Guide = false;

		public bool Capture = false;

		public bool DUp = false;

		public bool DDown = false;

		public bool DLeft = false;

		public bool DRight = false;
	}
}
