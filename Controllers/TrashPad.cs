using HIDuctTape.Controllers.Handlers;
using HIDuctTape.Controllers.Mapping;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace HIDuctTape.Controllers
{
	public class TrashPad
	{
		public MappingConfig Mapping;

		private List<SwitchController> SwitchControllers;

		private Type type = typeof(TrashPad);

		public ControllerState State = new ControllerState();

		public int Player;

		public BlockingCollection<InputEvent> InputQueue = new BlockingCollection<InputEvent>();

		public event EventHandler<InputEvent> OutputHandlers;

		public TrashPad(string configPath, int player = 1)
		{
			Mapping = new MappingConfig(JObject.Parse(File.ReadAllText(configPath)));
			Player = player;
			if (GlobalConfig.Current.UseSwitch)
			{
				SwitchControllers = new List<SwitchController>();
				foreach (SwitchConfig item in GlobalConfig.Current.SwitchConfigs.Where(c => c.Player == Player))
				{
					SwitchController switchController = new SwitchController(item);
					SwitchControllers.Add(switchController);
					OutputHandlers += switchController.Update;
				}
			}
		}

		public void ProcessQueue(object sender)
		{
			while (true)
			{
				foreach (InputEvent item in InputQueue.GetConsumingEnumerable())
				{
					InputEvent inputEvent = Mapping.Map(item);
					if (inputEvent != null)
					{
						if (inputEvent.OutProp.FieldType == typeof(bool))
						{
							inputEvent.OutProp.SetValue(State, Convert.ToBoolean(inputEvent.Value));
						}
						else
						{
							inputEvent.OutProp.SetValue(State, inputEvent.Value);
						}
						Log.Information(inputEvent.Name);
						if (this.OutputHandlers != null)
						{
							this.OutputHandlers(this, inputEvent);
						}
					}
				}
				Thread.Sleep(5);
			}
		}

		public void Disconnect()
		{
			foreach (SwitchController switchController in SwitchControllers)
			{
				switchController.Detach();
			}
		}
	}
}
