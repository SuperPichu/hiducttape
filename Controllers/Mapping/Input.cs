using System.Collections.Generic;
using HIDuctTape.Controllers.Handlers;
using HIDuctTape.Processors;
using Newtonsoft.Json;

namespace HIDuctTape.Controllers.Mapping
{
    public class Input
    {
        [JsonConverter(typeof(InputTypeJsonConverter))]
        public InputType Type { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Out { get; set; }
        public Output Output {get; set;}
        public ProcessChain<InputEvent> InputHandlers = new ProcessChain<InputEvent>();
    }
}