using HIDuctTape.Controllers.Mapping;
using HIDuctTape.Controllers.Handlers;
using HIDuctTape.Processors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HIDuctTape.Controllers.Mapping
{
	public class MappingConfig
	{
		private JObject Raw;
		private List<Input> Inputs = new List<Input>();
		private List<Output> Outputs = new List<Output>();
		public List<ControllerDef> Controllers  = new List<ControllerDef>();

		public MappingConfig(JObject raw)
		{
			Raw = raw;
			JArray controllers = (JArray)raw["controllers"];
			JArray inputs = (JArray)raw["in"];
			JArray outputs = (JArray)raw["out"];
			foreach(JObject jController in controllers){
				ControllerDef controller = jController.ToObject<ControllerDef>();
				Controllers.Add(controller);
			}
            foreach (JObject jOutput in outputs)
            {
				Output output = jOutput.ToObject<Output>();
				output.OutProp = typeof(ControllerState).GetField(output.Name);
                Outputs.Add(output);
            }
            foreach (JObject jInput in inputs)
            {
				Input input = jInput.ToObject<Input>();
                ControllerDef controller = Controllers.First(c => c.Name == input.Controller);
                if (input.Name != null)
                {
                    if (input.Type == InputType.Button) input.Index = controller.ButtonNames.IndexOf(input.Name);
                    if (input.Type == InputType.Axis) input.Index = controller.AxisNames.IndexOf(input.Name);
				}else {
					if(input.Type == InputType.Button && input.Index < controller.ButtonNames.Count){
						input.Name = controller.ButtonNames[input.Index];
					} else if (input.Type == InputType.Axis && input.Index < controller.AxisNames.Count)
                    {
                        input.Name = controller.AxisNames[input.Index];
                    }else {
                        input.Name = Enum.GetName(typeof(InputType), input.Type) + input.Index;
                    }
                }
				input.Output = Outputs.First(o => o.Name == input.Out);
				if(input.Type == InputType.Axis && input.Output.Type == InputType.Button){
					input.InputHandlers.AddProcessor(new AxisToButton(32767,1));
				}
                Inputs.Add(input);
            }
            

			// foreach (JProperty item in raw.Properties())
			// {
			// 	Console.WriteLine(item.Name);
			// 	ProcessChain<InputEvent> processChain = new ProcessChain<InputEvent>();
			// 	FieldInfo field = typeof(ControllerState).GetField(item.Name);
			// 	Console.WriteLine(field.Name);
			// 	JObject val = (JObject)item.Value;
			// 	string text = val["type"].ToString();
			// 	text = char.ToUpper(text[0]).ToString() + text.Substring(1);
			// 	InputType inputType = (InputType)Enum.Parse(typeof(InputType), text);
			// 	int num = (int)val["index"];
			// 	string key = text + num.ToString();
			// 	if (inputType == InputType.Axis)
			// 	{
			// 		string a = val["method"].ToString();
			// 		if (field.FieldType == typeof(bool))
			// 		{
			// 			short max = (short)val["max"];
			// 			short min = (short)val["min"];
			// 			processChain.AddProcessor(new AxisToButton(max, min));
			// 		}
			// 		if (a == "invert")
			// 		{
			// 			Console.WriteLine("invert");
			// 			processChain.AddProcessor(new Invert());
			// 		}
			// 		if (a == "range")
			// 		{
			// 			short max2 = (short)val["max"];
			// 			short min2 = (short)val["min"];
			// 			processChain.AddProcessor(new HIDuctTape.Processors.Range(max2, min2));
			// 		}
			// 		if (a == "multiaxis")
			// 		{
			// 			int axis = (int)val["axes"][0]["index"];
			// 			string type = val["axes"][0]["type"].ToString();
            //             int axis2 = (int)val["axes"][1]["index"];
			// 			string type2 = val["axes"][1]["type"].ToString();
			// 			processChain.AddProcessor(new MultiAxis(axis, type, axis2, type2));
			// 			OutProps.Add(text + axis.ToString(), field);
			// 			InputHandlers.Add(text + axis.ToString(), processChain);
			// 			OutProps.Add(text + axis2.ToString(), field);
			// 			InputHandlers.Add(text + axis2.ToString(), processChain);
			// 		}
			// 	}
			// 	if (inputType == InputType.Button && field.FieldType == typeof(short))
			// 	{
			// 		short on = short.MaxValue;
			// 		short off = 0;
			// 		bool flag = false;
			// 		if (val["on"] != null)
			// 		{
			// 			on = (short)val["on"];
			// 		}
			// 		if (val["off"] != null)
			// 		{
			// 			off = (short)val["off"];
			// 		}
			// 		if (val["multi"] != null)
			// 		{
			// 			flag = (bool)val["multi"];
			// 		}
			// 		processChain.AddProcessor(new ButtonsToAxis(off, on, flag));
			// 		if (flag)
			// 		{
			// 			OutProps.Add(text + on.ToString(), field);
			// 			InputHandlers.Add(text + on.ToString(), processChain);
			// 			OutProps.Add(text + off.ToString(), field);
			// 			InputHandlers.Add(text + off.ToString(), processChain);
			// 		}
			// 	}
			// 	if (inputType == InputType.Hat)
			// 	{
			// 		key = text + num.ToString() + val["direction"].ToString();
			// 	}
			// 	if (num > -1)
			// 	{
			// 		OutProps.Add(key, field);
			// 		InputHandlers.Add(key, processChain);
			// 	}
			// }
		}

		public InputEvent Map(InputEvent iEvent){
			if (Inputs.Where(i => i.Name == iEvent.Name).Count() == 0)
			{
				return null;
			}
			Input input = Inputs.First(i => i.Name == iEvent.Name);
			iEvent.OutProp = input.Output.OutProp;
			return input.InputHandlers.Execute(iEvent);
		}
	}
}
