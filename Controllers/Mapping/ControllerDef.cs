using System.Collections.Generic;
using HIDuctTape.Controllers.Handlers;

namespace HIDuctTape.Controllers.Mapping
{
    public class ControllerDef
    {
        public string Type { get; set; }
        public string Name { get; set; }

        public List<string> AxisNames {get; set;}
        public List<string> ButtonNames {get; set;}
    }
}