
using System;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace HIDuctTape.Controllers.Mapping
{
	public enum InputType
	{
		Axis,
		Button,
		Hat,
		Slider
	}

    public class InputTypeJsonConverter : JsonConverter<InputType>
    {
        public override InputType ReadJson(JsonReader reader, Type objectType, [AllowNull] InputType existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            InputType type;
            string name = Char.ToUpper(reader.Value.ToString().ToCharArray()[0]) + reader.Value.ToString().Substring(1);
            Enum.TryParse(name, out type);
			return type;
        }

        public override void WriteJson(JsonWriter writer, [AllowNull] InputType value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
