using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using HIDuctTape.Controllers.Handlers;
using Newtonsoft.Json;

namespace HIDuctTape.Controllers.Mapping
{
    public class Output
    {
        [JsonConverter(typeof(InputTypeJsonConverter))]
        public InputType Type { get; set; }
        public string Name { get; set; }

        public FieldInfo OutProp {get; set;}
    }

}