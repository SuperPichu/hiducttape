using HIDuctTape.Controllers.Handlers;
using Serilog;
using System;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace HIDuctTape.Controllers
{
	public class SwitchController
	{
		public readonly Socket Connection = new Socket(SocketType.Stream, ProtocolType.Tcp);

		public readonly string IP;

		public readonly int Port;

		public readonly int Player;

		private const int BaseDelay = 64;

		private const int DelayFactor = 256;

		public string Name
		{
			get;
			set;
		}

		public SwitchController(string name)
		{
			Name = name;
		}

		public SwitchController(string ip, int port)
		{
			IP = ip;
			Port = port;
			Player = 1;
			Connection.Connect(ip, port);
			Echo(on: true);
			if (Connection.Connected)
			{
				Console.WriteLine("I'm Alive!");
			}
		}

		public SwitchController(SwitchConfig config)
		{
			IP = config.Ip;
			Port = config.Port;
			Player = config.Player;
			Connection.Connect(IP, Port);
			Echo(on: true);
			if (Connection.Connected)
			{
				Console.WriteLine("I'm Alive!");
			}
		}

		private static byte[] Encode(string command)
		{
			return Encoding.ASCII.GetBytes(command + "\r\n");
		}

		public int Read(byte[] buffer)
		{
			return Connection.Receive(buffer);
		}

		public int Send(byte[] buffer)
		{
			if (!Connection.Connected)
			{
				Connection.Connect(IP, Port);
			}
			return Connection.Send(buffer);
		}

		public int Detach()
		{
			return Send(Encode("detachController"));
		}

		public int Echo(bool on)
		{
			return Send(Encode($"configure echoCommands {(on ? 1 : 0)}"));
		}

		public byte[] Press(string button)
		{
			return Encode("press " + button.ToUpper());
		}

		public byte[] Release(string button)
		{
			return Encode("release " + button.ToUpper());
		}

		public byte[] SetStick(string stick, int x, int y)
		{
			return Encode($"setStick {stick.ToUpper()} {x} {y}");
		}

		public void Update(object sender, InputEvent inputEvent)
		{
			FieldInfo outProp = inputEvent.OutProp;
			short value = inputEvent.Value;
			byte[] array = null;
			string text = outProp.Name;
			if (outProp.FieldType == typeof(bool))
			{
				if (text == "RightStick")
				{
					text = "RStick";
				}
				if (text == "LeftStick")
				{
					text = "LStick";
				}
				if (text == "Guide")
				{
					text = "Home";
				}
				if (text == "Start")
				{
					text = "Plus";
				}
				if (text == "Back")
				{
					text = "Minus";
				}
				Log.Information(text);
				array = ((!Convert.ToBoolean(value)) ? Release(text) : Press(text));
			}
			else if (outProp.FieldType == typeof(short))
			{
				if (outProp.Name.StartsWith("L"))
				{
					array = SetStick("Left", ((TrashPad)sender).State.LX, ((TrashPad)sender).State.LY);
				}
				else if (outProp.Name.StartsWith("R"))
				{
					array = SetStick("Right", ((TrashPad)sender).State.RX, ((TrashPad)sender).State.RY);
				}
			}
			if (array != null)
			{
				Send(array);
			}
		}
	}
}
