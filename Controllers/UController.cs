using HIDuctTape.Controllers.Handlers;
using System.Runtime.InteropServices;

namespace HIDuctTape.Controllers
{
	public class UController
	{
		private int fd;

		public UController(string name)
		{
			fd = SetupDevice(name.ToCharArray());
		}

		public void Update(object sender, InputEvent inputEvent)
		{
			int field = 0;
			int num = map(inputEvent.Value);
			int type = 0;
			switch (inputEvent.OutProp.Name)
			{
			case "A":
				field = 304;
				type = 1;
				break;
			case "B":
				field = 305;
				type = 1;
				break;
			case "X":
				field = 308;
				type = 1;
				break;
			case "Y":
				field = 307;
				type = 1;
				break;
			case "LeftShoulder":
				field = 310;
				type = 1;
				break;
			case "RightShoulder":
				field = 311;
				type = 1;
				break;
			case "Start":
				field = 315;
				type = 1;
				break;
			case "Back":
				field = 314;
				type = 1;
				break;
			case "LeftStick":
				field = 317;
				type = 1;
				break;
			case "RightStick":
				field = 318;
				type = 1;
				break;
			case "DLeft":
				field = 16;
				type = 3;
				if (num > 0)
				{
					num = -512;
				}
				break;
			case "DRight":
				field = 16;
				type = 3;
				if (num > 0)
				{
					num = 512;
				}
				break;
			case "DUp":
				field = 16;
				type = 3;
				if (num > 0)
				{
					num = 512;
				}
				break;
			case "DDown":
				field = 16;
				type = 3;
				if (num > 0)
				{
					num = -512;
				}
				break;
			case "Guide":
				field = 316;
				type = 1;
				break;
			case "LX":
				field = 0;
				type = 3;
				break;
			case "LY":
				field = 1;
				type = 3;
				break;
			case "RX":
				field = 3;
				type = 3;
				break;
			case "RY":
				field = 4;
				type = 3;
				break;
			case "LT":
				field = 2;
				type = 3;
				break;
			case "RT":
				field = 5;
				type = 3;
				break;
			}
			Update(fd, type, field, num);
			Sync(fd);
		}

		private static int map(int value)
		{
			return (value - -32768) * 1024 / 65535 + 512;
		}

		[DllImport("./libvirtpad.so", EntryPoint = "setupDevice", SetLastError = true)]
		private static extern int SetupDevice(char[] name);

		[DllImport("./libvirtpad.so", EntryPoint = "update", SetLastError = true)]
		private static extern int Update(int fd, int type, int field, int value);

		[DllImport("./libvirtpad.so", EntryPoint = "syncUpdate", SetLastError = true)]
		private static extern int Sync(int fd);
	}
}
