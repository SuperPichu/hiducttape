using HIDuctTape.Controllers.Mapping;
using HIDuctTape.Gamepad;
using Serilog;
using System;

namespace HIDuctTape.Controllers.Handlers
{
	public class GamepadHandler : InputHandler<GamepadController,ButtonEvent,AxisEvent>
	{
		private GamepadController Gamepad;

		private ControllerState State = new ControllerState();

		public GamepadHandler(TrashPad trashPad, GamepadController gamepad)
			: base(trashPad)
		{
			Gamepad = gamepad;
			Gamepad.ButtonChanged += ProcessButton;
			Log.Information("here");
			Gamepad.AxisChanged += ProcessAxis;
		}

		public override void ProcessButton(object sender, ButtonEvent e)
		{
            Log.Information(Gamepad.ButtonNames[e.Button]);
			TrashPad.InputQueue.Add(new InputEvent(InputType.Button, e.Button, Convert.ToInt16(e.Pressed),Gamepad.ButtonNames[e.Button]));
		}

		public override void ProcessAxis(object sender, AxisEvent e)
		{
			string text = "";
			if (e.Axis < Gamepad.AxisNames.Count)
			{
				text = Gamepad.AxisNames[e.Axis];
				if (text.ToLower().Contains('x') && Gamepad.Name.Contains("360"))
				{
					text = text.Replace('x', 'y');
					text = text.Replace('X', 'Y');
				}
				else if (text.ToLower().Contains('y') && Gamepad.Name.Contains("360"))
				{
					text = text.Replace('y', 'x');
					text = text.Replace('Y', 'X');
				}
				Log.Information(text);
			}
			InputEvent item = new InputEvent(InputType.Axis, e.Axis, e.Value,text);
			TrashPad.InputQueue.Add(item);
		}
	}
}
