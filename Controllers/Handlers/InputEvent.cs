using System;
using System.Reflection;
using HIDuctTape.Controllers.Mapping;

namespace HIDuctTape.Controllers.Handlers
{
	public class InputEvent : EventArgs
	{
		public string Name;

		private InputType Type;

		public short Value;

		public int Index
		{
			get;
			set;
		}

		public FieldInfo OutProp
		{
			get;
			set;
		}

		public InputEvent(InputType type, int index, short value = 0, string name="")
		{
			Type = type;
			Value = value;
			Index = index;
			Name = name;
			if(name == "") Name = Enum.GetName(typeof(InputType), type) + index.ToString();
		}
	}
}
