using System;

namespace HIDuctTape.Controllers.Handlers
{
    public abstract class InputHandler<TDevice,TBEvent,TAEvent>
    {
		protected TrashPad TrashPad;
		public InputHandler(TrashPad trashPad){
			TrashPad = trashPad;
		}
		protected void UpdatePoller(TDevice device){
			throw new NotImplementedException();
		}
        public abstract void ProcessAxis(object sender, TAEvent e);
        public abstract void ProcessButton(object sender, TBEvent e);
    }
}
