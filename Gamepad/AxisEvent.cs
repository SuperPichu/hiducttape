namespace HIDuctTape.Gamepad
{
	public class AxisEvent
	{
		public byte Axis
		{
			get;
			set;
		}

		public short Value
		{
			get;
			set;
		}
	}
}
