using System;

namespace HIDuctTape.Gamepad
{
	public interface IGamepadController : IDisposable
	{
		event EventHandler<ButtonEvent> ButtonChanged;

		event EventHandler<AxisEvent> AxisChanged;
	}
}
