namespace HIDuctTape.Gamepad
{
	public class ButtonEvent
	{
		public byte Button
		{
			get;
			set;
		}

		public bool Pressed
		{
			get;
			set;
		}
	}
}
