using HIDuctTape.Controllers.Mapping;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tmds.Linux;
using static Tmds.Linux.LibC;
using static HIDuctTape.Utils;

namespace HIDuctTape.Gamepad
{
	public class GamepadController : IGamepadController, IDisposable
	{
		private readonly string _deviceFile;

		private readonly CancellationTokenSource _cancellationTokenSource;

		public Dictionary<byte, bool> Buttons = new Dictionary<byte, bool>();

		public Dictionary<byte, short> Axis = new Dictionary<byte, short>();

		public string Name
		{
			get;
			set;
		}

		public List<string> AxisNames
		{
			get;
			set;
		}
		public List<string> ButtonNames
		{
			get;
			set;
		}

		public event EventHandler<ButtonEvent> ButtonChanged;

		public event EventHandler<AxisEvent> AxisChanged;

		public GamepadController(string deviceFile,ControllerDef controller)
		{
			if (!File.Exists(deviceFile))
			{
				throw new ArgumentException("deviceFile", "The device " + deviceFile + " does not exist");
			}
			Name = controller.Name;
			AxisNames = controller.AxisNames;
			ButtonNames = controller.ButtonNames;
			_deviceFile = deviceFile;
			_cancellationTokenSource = new CancellationTokenSource();
			Task.Factory.StartNew((Action)delegate
			{
				ProcessMessages(_cancellationTokenSource.Token);
			});
		}

		private void ProcessMessages(CancellationToken token)
		{
			using (FileStream fileStream = new FileStream(_deviceFile, FileMode.Open))
			{
				byte[] array = new byte[8];
				while (!token.IsCancellationRequested)
				{
					fileStream.Read(array, 0, 8);
					if (array.HasConfiguration())
					{
						ProcessConfiguration(array);
					}
					ProcessValues(array);
				}
			}
		}

		private void ProcessConfiguration(byte[] message)
		{
			if (message.IsButton())
			{
				byte address = message.GetAddress();
				if (!Buttons.ContainsKey(address))
				{
					Buttons.Add(address, value: false);
				}
			}
			else if (message.IsAxis())
			{
				byte address2 = message.GetAddress();
				if (!Axis.ContainsKey(address2))
				{
					Axis.Add(address2, 0);
				}
			}
			else
			{
				Console.WriteLine(Encoding.ASCII.GetString(message));
			}
		}

		private void ProcessValues(byte[] message)
		{
			if (message.IsButton())
			{
				bool flag = Buttons[message.GetAddress()];
				bool flag2 = message.IsButtonPressed();
				if (flag != flag2)
				{
					Buttons[message.GetAddress()] = message.IsButtonPressed();
					this.ButtonChanged?.Invoke(this, new ButtonEvent
					{
						Button = message.GetAddress(),
						Pressed = flag2
					});
				}
			}
			else if (message.IsAxis())
			{
				short num = Axis[message.GetAddress()];
				short axisValue = message.GetAxisValue();
				if (num != axisValue)
				{
					Axis[message.GetAddress()] = message.GetAxisValue();
					this.AxisChanged?.Invoke(this, new AxisEvent
					{
						Axis = message.GetAddress(),
						Value = axisValue
					});
				}
			}
		}

		public static string GetName(string device){
			string resp = Bash($"udevadm info {device}");
			return resp.Split("ID_MODEL=")[1].Split("\n")[0].Replace('_',' ');
		}

		public void Dispose()
		{
			_cancellationTokenSource.Cancel();
		}
	}
}
