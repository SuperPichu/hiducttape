using System;

namespace HIDuctTape.Gamepad
{
	public static class ByteArrayExtensions
	{
		public static bool HasConfiguration(this byte[] message)
		{
			return IsFlagSet(message[6], 128);
		}

		public static bool IsButton(this byte[] message)
		{
			return IsFlagSet(message[6], 1);
		}

		public static bool IsAxis(this byte[] message)
		{
			return IsFlagSet(message[6], 2);
		}

		public static bool IsButtonPressed(this byte[] message)
		{
			return message[4] == 1;
		}

		public static byte GetAddress(this byte[] message)
		{
			return message[7];
		}

		public static short GetAxisValue(this byte[] message)
		{
			return BitConverter.ToInt16(new byte[2]
			{
				message[4],
				message[5]
			}, 0);
		}

		private static bool IsFlagSet(byte value, byte flag)
		{
			byte b = (byte)(value & flag);
			return b == flag;
		}
	}
}
