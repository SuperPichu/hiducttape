from evdev import InputDevice,UInput,ecodes,InputEvent
import socket

controllers = []
caps = {
    ecodes.EV_ABS: [ecodes.ABS_X,ecodes.ABS_Y,ecodes.ABS_RX,ecodes.ABS_RY,ecodes.ABS_Z,ecodes.ABS_RZ],
    ecodes.EV_KEY: [ecodes.BTN_A,ecodes.BTN_B,ecodes.BTN_X,ecodes.BTN_Y,ecodes.BTN_START,ecodes.BTN_BACK,ecodes.BTN_TL,ecodes.BTN_TR,ecodes.BTN_THUMBL,ecodes.BTN_THUMBR,ecodes.BTN_MODE]
}

mapping = {
    "A":ecodes.BTN_A,
    "B":ecodes.BTN_B,
    "X":ecodes.BTN_X,
    "Y":ecodes.BTN_Y,
    "Start":ecodes.BTN_START,
    "Back":ecodes.BTN_BACK,
    "Home":ecodes.BTN_MODE,
    "L":ecodes.BTN_TL,
    "R":ecodes.BTN_TR,
    "RightStick":ecodes.BTN_THUMBR,
    "LeftStick":ecodes.BTN_THUMBL,
    "LX":ecodes.ABS_X,
    "LY":ecodes.ABS_Y,
    "RX":ecodes.ABS_RX,
    "RY":ecodes.ABS_RY,
    "Z":ecodes.ABS_Z,
    "RZ":ecodes.ABS_RZ,
}
device = None
HOST = ''
PORT = 6002
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()
    with conn:
        while True:
            data = str(conn.recv(1024),encoding='ascii').split(' ')
            if not data: break
            cmd = data[0]
            if cmd == 'setup':
                players = int(data[1])
                for x in range(players):
                    controller = UInput(caps,"TrashPad " + str(x+1))
                    controllers.append(controller)
            else:
                player = int(data[1])
                controller = controllers[player-1]
                field = mapping[data[2]]
                if cmd == "press":
                    controller.write(ecodes.EV_KEY,field,1)
                elif cmd == "release":
                    controller.write(ecodes.EV_KEY,field,0)
                elif cmd == "axis":
                    controller.write(ecodes.EV_ABS,field,int(data[3]))
                controller.syn()